---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

<ul>
{% for f in site.data.foo %}
  <li>
    {{ f.title }}
    {{ f.description }}
{% for i in f.inner %}
  <li>
    {{ i.title }}
    {{ i.description }}
  </li>
{% endfor %}
  </li>
{% endfor %}
</ul>


{% include nested_accordion.html %}


**List of posts**  
  
{% for p in site.posts %}  
{% capture details %}  
  {{ p.excerpt }}  
  {% capture details %}  
  {{ p.url }}  
  {% endcapture %}  
  {% capture summary %}{{ p.path }}{% endcapture %}{% include details.html %}  
  {% endcapture %}  
{% capture summary %}{{ p.title }}{% endcapture %}{% include details.html %}  
{% endfor %}  

